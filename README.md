# Conda fasd recipe

Package that provides via conda the [[fasd](https://github.com/clvv/fasd "fasd project")] 
tool.

## Create package
``` sh
conda build -c conda-forge .
```
